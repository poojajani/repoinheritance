package inhritance_pckg;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Change_password_child2 extends Child_class {
	
	@BeforeTest
	  public void beforeTest() {
	  }
	
  @Test(priority=5)
  /*public void all_value_blank() throws InterruptedException {
	 Thread.sleep(10000);
	driver.findElement(By.xpath("//a[text()='Change Password']")).click();
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(5000);
	  String actual = driver.findElement(By.xpath("//div[@id='passwordBlank']")).getText();
	  String expected = "Current password cannot be empty";
	  Assert.assertEquals(actual, expected);
  }
  
  @Test(priority=6)
  public void Invalid_current_pass() throws InterruptedException {
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//input[@id='passwordc']")).sendKeys("pooja123");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  Thread.sleep(5000);
	  String actual = driver.findElement(By.xpath("//div[@id='password1Blank']")).getText();
	  String expected = "New password cannot be empty";
	  Assert.assertEquals(actual, expected);
  }
  
  @Test(priority=7)
  public void samll_new_pass_validation() throws InterruptedException {
	  driver.findElement(By.xpath("//input[@id='password1']")).sendKeys("123");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  Thread.sleep(5000);
	  String actual = driver.findElement(By.xpath("//div[text()='Password must be 6 characters or more']")).getText();
	  String expected = "Password must be 6 characters or more";
	  Assert.assertEquals(actual, expected);
  }
  
 @Test(priority=8)
  public void _new_pass_() throws InterruptedException {
	  driver.findElement(By.xpath("//input[@id='password1']")).clear();
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//input[@id='password1']")).sendKeys("pooja12345678");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  String actual = driver.findElement(By.xpath("//div[@id='password2Blank']")).getText();
	  String expected = "Confirm password cannot be empty";
	  Assert.assertEquals(actual, expected);
  }

  @Test(priority=9)
  public void new_and_confirnpass_differ() throws InterruptedException {
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//input[@id='password2']")).sendKeys("poojapratik");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  Thread.sleep(5000);
	  String actual = driver.findElement(By.xpath("//div[text()='New password and confirm new password do not match']")).getText();
	  String expected = "New password and confirm new password do not match";
	  Assert.assertEquals(actual, expected);
  }
  
  @Test(priority=10)
  public void valid_confirm_pass() throws InterruptedException {
	  driver.findElement(By.xpath("//input[@id='password2']")).clear();
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//input[@id='password2']")).sendKeys("pooja12345678");
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
  }
  */
  @AfterTest
  public void afterTest() {
  }

}
